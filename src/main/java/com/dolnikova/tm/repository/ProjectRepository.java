package com.dolnikova.tm.repository;

import com.dolnikova.tm.Action;
import com.dolnikova.tm.Message;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.*;

public class ProjectRepository implements Action {

    private Map<String, Project> projects = new LinkedHashMap<>();

    @Override
    public List<Project> findAll() {
        List<Project> allProjects = new ArrayList<>();
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            allProjects.add(project.getValue());
        }
        return allProjects;
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public Project findOne(String name) {
        for(Map.Entry<String, Project> project : projects.entrySet()) {
            Project foundProject = project.getValue();
            if (name.equals(foundProject.getName())) return foundProject;
        }
        return null;
    }

    public void merge(String name, Project project) {
        project.setName(name);
    }

    @Override
    public void remove(String id) {
        projects.remove(id);
    }

    public Map<String, Project> getProjects() {
        return projects;
    }
}
