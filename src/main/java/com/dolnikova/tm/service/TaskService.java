package com.dolnikova.tm.service;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TaskService {

    private TaskRepository taskRepository;
    private Scanner scanner = new Scanner(System.in);

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        List<Task> allTasks = taskRepository.findAll();
        if (allTasks.isEmpty()) {
            System.out.println(Message.NO_TASKS);
            return null;
        }
        for (Task task : allTasks) {
            System.out.println(task.getName());
        }
        return allTasks;
    }

    public Task findOne() {
        Project foundProject = findProject();
        if (foundProject == null) return null;
        ArrayList<Task> projectTasks = getTasksByProjectId(foundProject.getId());
        System.out.println(Message.IN_PROJECT + foundProject.getName() + " " + projectTasks.size() + " " + Message.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Message.INSERT_TASK_ID);
        Task task = null;
        while (task == null) {
            String taskId = scanner.nextLine();
            if (taskId.isEmpty()) break;
            task = taskRepository.findOne(taskId);
            if (task == null) System.out.println(Message.INCORRECT_NUMBER);
            else System.out.println(Message.ID + Message.COLON + taskId + " " + task.getName());
        }
        return null;
    }

    public ArrayList<Task> getTasksByProjectId(String projectId) {
        ArrayList<Task> projectTasks = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskRepository.getTasks().entrySet()) {
            if (entry.getValue().getProjectId().equals(projectId)) projectTasks.add(entry.getValue());
        }
        return projectTasks;
    }


    public void persist() {
        if (Bootstrap.projectService.getProjectRepository().getProjects().isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return;
        }
        System.out.println(Message.CHOOSE_PROJECT);
        Project foundProject = null;
        while (foundProject == null) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            foundProject = Bootstrap.projectService.getProjectRepository().findOne(projectName);
            if (foundProject == null) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
        }
        if (foundProject == null) return;
        System.out.println(Message.INSERT_TASK);
        boolean taskCreationCompleted = false;
        while (!taskCreationCompleted) {
            String taskText = scanner.nextLine();
            if (taskText.isEmpty()) taskCreationCompleted = true;
            else {
                Task newTask = new Task(foundProject.getId(), taskText);
                taskRepository.persist(newTask);
                System.out.println(Message.TASK + " " + taskText + " " + Message.CREATED_F);
            }
        }
        System.out.println(Message.TASK_ADDITION_COMPLETED);
    }

    public void merge() {
        Project project = findProject();
        ArrayList<Task> projectTasks = getTasksByProjectId(project.getId());
        int taskSize = projectTasks.size();
        if (taskSize == 0) {
            System.out.println(Message.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Message.IN_PROJECT + project.getName() + " " + taskSize + " " + Message.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Message.INSERT_TASK_ID);
        boolean taskUpdated = false;
        while (!taskUpdated) {
            String taskIdToUpdate = scanner.nextLine();
            if (taskIdToUpdate.isEmpty()) break;
            System.out.println(Message.INSERT_NEW_TASK);
            String newTaskText = scanner.nextLine();
            if (newTaskText.isEmpty()) break;
            Task taskToAdd = new Task(project.getId(), newTaskText);
            taskRepository.merge(taskIdToUpdate, taskToAdd);
            System.out.println(Message.TASK_UPDATED);
            taskUpdated = true;
        }
    }

    public void remove() {
        Project project = findProject();
        if (project == null) return;
        ArrayList<Task> projectTasks = getTasksByProjectId(project.getId());
        int taskSize = projectTasks.size();
        if (taskSize == 0) {
            System.out.println(Message.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Message.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + Message.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Message.INSERT_TASK_ID);
        boolean taskDeleted = false;
        while (!taskDeleted) {
            String taskIdToDelete = scanner.nextLine();
            if (taskIdToDelete.isEmpty()) break;
            taskRepository.remove(taskIdToDelete);
            System.out.println(Message.TASK_DELETED);
            taskDeleted = true;
        }
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Project findProject() {
        if (Bootstrap.projectService.getProjectRepository().getProjects().isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return null;
        }
        System.out.println(Message.CHOOSE_PROJECT);
        Project foundProject = null;
        while (foundProject == null) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            foundProject = Bootstrap.projectService.getProjectRepository().findOne(projectName);
            if (foundProject == null) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
        }
        return foundProject;
    }

}
