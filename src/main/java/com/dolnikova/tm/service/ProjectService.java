package com.dolnikova.tm.service;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ProjectService {

    private ProjectRepository projectRepository;
    private Scanner scanner = new Scanner(System.in);

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll() {
        List<Project> allProjects = projectRepository.findAll();
        if (allProjects.isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return null;
        }
        for (Project project : allProjects) {
            System.out.println(project.getName());
        }
        return allProjects;
    }

    public Project findOne() {
        if (projectRepository.getProjects().isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return null;
        }
        System.out.println(Message.INSERT_PROJECT_NAME);
        Project project = null;
        while (project == null) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = projectRepository.findOne(projectName);
            if (project == null) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
            else System.out.println(Message.PROJECT_NAME + project.getName());
        }
        return project;
    }

    public void persist() {
        System.out.println(Message.INSERT_NEW_PROJECT_NAME);
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()) return;
        for (Map.Entry<String, Project> project : projectRepository.getProjects().entrySet()) {
            if (projectName.equals(project.getValue().getName())) {
                System.out.println(Message.PROJECT_NAME_ALREADY_EXIST);
                return;
            }
        }
        Project newProject = new Project(projectName);
        projectRepository.persist(newProject);
        System.out.println(Message.PROJECT + " " + projectName + " " + Message.CREATED_M);
    }

    public void merge() {
        System.out.println(Message.INSERT_PROJECT_NAME);
        Project foundProject = null;
        while (foundProject == null) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) return;
            for (Map.Entry<String, Project> project : projectRepository.getProjects().entrySet()) {
                foundProject = project.getValue();
                if (projectName.equals(foundProject.getName())) break;
                foundProject = null;
            }
            if (foundProject != null) {
                System.out.println(Message.INSERT_NEW_PROJECT_NAME);
                boolean newNameInserted = false;
                while (!newNameInserted) {
                    String newProjectName = scanner.nextLine();
                    if (newProjectName.isEmpty()) return;
                    projectRepository.merge(newProjectName, foundProject);
                    System.out.println(Message.PROJECT_UPDATED);
                    newNameInserted = true;
                }
            } else System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + Message.TRY_AGAIN);
        }
    }

    public void remove() {
        if (projectRepository.getProjects().isEmpty()) {
            System.out.println(Message.NO_PROJECTS);
            return;
        }
        System.out.println(Message.WHICH_PROJECT_DELETE);
        Project project = selectProject();
        if (project == null) System.out.println(Message.PROJECT_NAME_DOESNT_EXIST);
        else {
            projectRepository.remove(project.getId());
            System.out.println(Message.PROJECT_DELETED);
        }
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for(Map.Entry<String, Project> project : projectRepository.getProjects().entrySet()) {
                Project foundProject = project.getValue();
                if (projectName.equals(foundProject.getName())) return foundProject;
            }
            System.out.println(Message.PROJECT_NAME_DOESNT_EXIST + " " + Message.TRY_AGAIN);
        }
    }
}
