package com.dolnikova.tm;

import com.dolnikova.tm.entity.Entity;

import java.util.List;

public interface Action {

    public List findAll();

    public Entity findOne(String id);

    public void remove(String id);

    public void removeAll();

}
