package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.Message;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    public static ProjectService projectService;
    public static TaskService taskService;

    public void init() {
        System.out.println(Message.WELCOME);
        Scanner scanner = new Scanner(System.in);

        ProjectRepository projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        TaskRepository taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);

        String input = "exit";
        while (!input.isEmpty()) {
            input = scanner.nextLine();
            switch (input) {
                case Message.HELP:
                    showHelp();
                    break;
                case Message.FIND_ALL_PROJECTS:
                    projectService.findAll();
                    break;
                case Message.FIND_PROJECT:
                    projectService.findOne();
                    break;
                case Message.PERSIST_PROJECT:
                    projectService.persist();
                    break;
                case Message.MERGE_PROJECT:
                    projectService.merge();
                    break;
                case Message.REMOVE_PROJECT:
                    projectService.remove();
                    break;
                case Message.REMOVE_ALL_PROJECTS:
                    projectService.removeAll();
                    break;
                case Message.FIND_ALL_TASKS:
                    taskService.findAll();
                    break;
                case Message.FIND_TASK:
                    taskService.findOne();
                    break;
                case Message.PERSIST_TASK:
                    taskService.persist();
                    break;
                case Message.MERGE_TASK:
                    taskService.merge();
                    break;
                case Message.REMOVE_TASK:
                    taskService.remove();
                    break;
                case Message.REMOVE_ALL_TASKS:
                    taskService.removeAll();
                    break;
                default:
                    if (!input.isEmpty())
                        System.out.println(Message.COMMAND_DOESNT_EXIST);
                    break;
            }
        }
        System.out.println(Message.APP_EXIT);
    }

    private static void showHelp() {
        System.out.println(Message.HELP_STRING);
    }

}
