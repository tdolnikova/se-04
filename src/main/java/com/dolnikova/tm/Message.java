package com.dolnikova.tm;

public class Message {

    /*
     * General messages
     * */

    public final static String WELCOME = "* * * Welcome to Task Manager * * *";
    public final static String APP_EXIT = "Выход из программы.";
    public final static String COMMAND_DOESNT_EXIST = "Такой команды не существует";
    public final static String ID = "ID";
    public final static String COLON = ": ";
    public final static String INCORRECT_NUMBER = "Некорректный ввод числа.";
    public final static String TRY_AGAIN = "Попробуйте еще раз.";

    /*
     * Help messages
     * */

    public final static String HELP_STRING = Message.HELP + Message.COLON + Message.SHOW_ALL_COMMANDS + "\n"
            + Message.FIND_ALL_PROJECTS + Message.COLON + Message.FIND_ALL_PROJECTS + "\n"
            + Message.FIND_PROJECT + Message.COLON + Message.FIND_PROJECT + "\n"
            + Message.PERSIST_PROJECT + Message.COLON + Message.PERSIST_PROJECT + "\n"
            + Message.MERGE_PROJECT + Message.COLON + Message.MERGE_PROJECT + "\n"
            + Message.REMOVE_PROJECT + Message.COLON + Message.REMOVE_PROJECT + "\n"
            + Message.REMOVE_ALL_PROJECTS + Message.COLON + Message.REMOVE_ALL_PROJECTS + "\n"
            + Message.FIND_ALL_TASKS + Message.COLON + Message.FIND_ALL_TASKS + "\n"
            + Message.FIND_TASK + Message.COLON + Message.FIND_TASK + "\n"
            + Message.PERSIST_TASK + Message.COLON + Message.PERSIST_TASK + "\n"
            + Message.MERGE_TASK + Message.COLON + Message.MERGE_TASK + "\n"
            + Message.REMOVE_TASK + Message.COLON + Message.REMOVE_TASK + "\n"
            + Message.REMOVE_ALL_TASKS + Message.COLON + Message.REMOVE_ALL_TASKS + "\n"
            + Message.PRESS_ENTER + Message.COLON + Message.EXIT;

    public final static String HELP = "help";
    public final static String SHOW_ALL_COMMANDS = "show all commands";

    public final static String FIND_ALL_PROJECTS = "find all projects";
    public final static String FIND_PROJECT = "find project";
    public final static String PERSIST_PROJECT = "create project";
    public final static String MERGE_PROJECT = "update project";
    public final static String REMOVE_PROJECT = "delete project";
    public final static String REMOVE_ALL_PROJECTS = "delete all projects";

    public final static String FIND_ALL_TASKS = "find all tasks";
    public final static String FIND_TASK = "find task";
    public final static String PERSIST_TASK = "create task";
    public final static String MERGE_TASK = "update task";
    public final static String REMOVE_TASK = "delete task";
    public final static String REMOVE_ALL_TASKS = "delete all tasks";

    public final static String PRESS_ENTER = ">> press enter <<";
    public final static String EXIT = "exit";

    /*
    * Project messages
    * */

    public final static String CHOOSE_PROJECT = "Выберите проект";
    public final static String PROJECT_NAME = "Название проекта: ";
    public final static String IN_PROJECT = "В проекте ";
    public final static String PROJECT = "Проект";
    public final static String CREATED_M = "создан.";
    public final static String NO_PROJECTS = "Проектов нет.";
    public final static String PROJECT_UPDATED = "Проект обновлен.";
    public final static String PROJECT_DELETED = "Проект удален.";
    public final static String INSERT_PROJECT_NAME = "Введите название проекта.";
    public final static String PROJECT_NAME_DOESNT_EXIST = "Проекта с таким названием не существует.";
    public final static String PROJECT_NAME_ALREADY_EXIST = "Проект с таким названием уже существует.";
    public final static String WHICH_PROJECT_DELETE = "Какой проект хотите удалить?";
    public final static String NO_TASKS_IN_PROJECT = "В выбранном проекте нет задач";

    /*
     * Task messages
     * */

    public final static String TASK = "Задача";
    public final static String NO_TASKS = "Нет задач.";
    public final static String TASK_ADDITION_COMPLETED = "Создание задач завершено.";
    public final static String CREATED_F = "создана.";
    public final static String INSERT_TASK = "Введите задачу: ";
    public final static String INSERT_NEW_TASK = "Введите новую задачу";
    public final static String INSERT_TASK_ID = "Введите id задачи.";
    public final static String INSERT_NEW_PROJECT_NAME = "Введите название нового проекта.";
    public final static String TASK_UPDATED = "Задача изменена.";
    public final static String TASK_DELETED = "Задача удалена.";
    public final static String OF_TASKS_WITH_POINT = "задач(и)";

}
